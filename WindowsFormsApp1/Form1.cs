﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public int numThreads; 
        public Form1()
        {
            InitializeComponent();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
        }
        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string filePath = openFileDialog.FileName;
                var csvFile = new CSVFile(filePath, @"ImageToCsv.csv", numThreads);
                csvFile.createCSVFile();
            }
        }

  
        private void button2_Click_1(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string filePath = openFileDialog.FileName;
                var csvFile = new CSVFile(filePath, @"test.csv", numThreads);
                csvFile.loadCSVFileAsDict(chart1);
            }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            try
            {
                numThreads = Int32.Parse(textBox2.Text);
            }
            catch
            {
                numThreads = 1;
            }
        }
    }

    public class CSVFile
    {
        
        public string path;
    
        
        public string csvFile;
        public int numberThreads;
        public static StringBuilder csvTest = new StringBuilder();
        private static string[] extensions = new string[3] { ".jpg", ".bmp", ".png" };
        static ReaderWriterLock rwl = new ReaderWriterLock();
        public static int reads = 0;
        public CSVFile(string photoPath, string newFilePath, int numThreads)
        {
            path = photoPath;
            csvFile = newFilePath;
            numberThreads = numThreads;
        }
        public static void ReadFromResource(int timeOut, string path, string output, int height, int fromHeight, Thread[] thread2, Bitmap img, int numberThreads)
        {
            try {
                rwl.AcquireReaderLock(timeOut);
                try {
                    MessageBox.Show(numberThreads + "WATKOW");
                    //MessageBox.Show(thread2[itt].Name.ToString()+"THREADS");
                    // It is safe for this thread to read from the shared resource.
                    if (extensions.Contains(Path.GetExtension(path).ToString()))
                    {
                       
                        lock(img){
                            for (int x = fromHeight; x < height; x++)
                            {
                                for (int y = 0; y < img.Width; y++)
                                {
                                    Color pixel = img.GetPixel(y, x);
                                    byte r = pixel.R;
                                    byte g = pixel.G;
                                    byte b = pixel.B;
                                    var newLine = string.Format("{0},{1},{2},{3},{4}", x, y, r, g, b);
                                    lock(csvTest){
                                        reads = reads + 1;
                                        csvTest.AppendLine(newLine);
                                    }
                                }
                            }
                            //MessageBox.Show(reads.ToString());
                        }
                    }
                    else
                    {
                        MessageBox.Show("Wrong file extension.");
                    }
                 }
                 finally {
                    // Ensure that the lock is released.
                    rwl.ReleaseReaderLock();
                 }
            }
            catch (ApplicationException) {
                // The reader lock request timed out.
            }
        }
        public static void ReadFromCsv(int timeOut, int fromLine, int endLine, List<string[]> list, int[,,] colorsMatrix){
            try{
                rwl.AcquireReaderLock(timeOut);
                try{
                    var file = new List<string[]>();
                    for(int x=fromLine; x < endLine; x++){
                         var line = list[x];
                         var r = Int32.Parse(line [2]);
                         var g = Int32.Parse(line [3]);
                         var b = Int32.Parse(line [4]);
                        lock(colorsMatrix){
                             colorsMatrix[r,g,b] += 1;
                        }
                    }

                }
                finally{
                    rwl.ReleaseReaderLock();
                }
            }
            catch (ApplicationException) {
                // The reader lock request timed out.
            }
        }
        public static void generateImageToCsvRaport(int numThreads, TimeSpan time){
            StringBuilder raportString = new StringBuilder();
            var raportLine = string.Format("{0},{1},{2}", numThreads, time, reads);

            raportString.AppendLine(raportLine);

            if(!File.Exists("raportImgToCsv.csv")){
                var firstLine = string.Format("CPU,Czas,Pixele{0}", Environment.NewLine);
                raportString.Insert(0,firstLine);
                File.WriteAllText("raportImgToCsv.csv", raportString.ToString());
            }
            else{
                File.AppendAllText("raportImgToCsv.csv", raportString.ToString());
            }
        }
         public static void generatePixogramRaport(int numThreads, TimeSpan time, int counter, int valueSum, string dictKey){
            StringBuilder raportString = new StringBuilder();
             var raportLine = string.Format("{0},{1},{2},{3},{4}", numThreads, time, counter, valueSum, dictKey);

            raportString.AppendLine(raportLine);

            if(!File.Exists("raportPixogram.csv")){
                var firstLine = string.Format("CPU,Czas,Pixele,Najczestszy{0}", Environment.NewLine);
                raportString.Insert(0,firstLine);
                File.WriteAllText("raportPixogram.csv", raportString.ToString());
            }
            else{
                File.AppendAllText("raportPixogram.csv", raportString.ToString());
            }
        }

        public void createCSVFile()
        {
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            int iCPU = System.Environment.ProcessorCount;
            csvTest.Clear();
            reads = 0;
            Bitmap img = new Bitmap(path);
         
            Thread[] threadArray = new Thread[numberThreads];
     
            for (int i = 0; i < numberThreads; i++){
                int heightPerThread = (img.Height / numberThreads);
                int diffheight = img.Height -  (img.Height / numberThreads);
                int fromHeight=0;
                if(numberThreads == 1){
                     fromHeight = 0;
                     heightPerThread = img.Height;
                }
                else if(i == 0 && numberThreads > 1)
                {
                    fromHeight = 0;
                    heightPerThread = (img.Height / numberThreads);
                }
                else if(i == numberThreads - 1){
                    fromHeight = heightPerThread * i;
                    heightPerThread = img.Height;
                }
                else{
                    fromHeight =  heightPerThread * i;
                    heightPerThread = ((img.Height / numberThreads) * (i+1));
                }
               Bitmap clone = (Bitmap) img.Clone();
              //var a = i;
               threadArray[i] = new Thread(()=>CSVFile.ReadFromResource(10, path, csvFile, heightPerThread, fromHeight, threadArray, clone, numberThreads));
               threadArray[i].Name = i + " Thread";
               threadArray[i].Start();
          
            }
            for (int i = 0; i < numberThreads; i++)
            {
                threadArray[i].Join();
            }
        
            stopWatch.Stop();
            File.WriteAllText(csvFile, csvTest.ToString());
            MessageBox.Show(stopWatch.Elapsed.ToString());
            CSVFile.generateImageToCsvRaport(numberThreads, stopWatch.Elapsed);
            
        }

        internal void loadCSVFileAsDict(System.Windows.Forms.DataVisualization.Charting.Chart chart)
        {
            //var colors = new Dictionary<string, Dictionary<int, int>>
            //{
            //    { "RED", new Dictionary<int, int>() },
            //    { "GREEN", new Dictionary<int, int>() },
            //    { "BLUE", new Dictionary<int, int>() },
            //};
            for(int x=1;x<=15;x++){
            int[,,] colorsMatrix = new int[256, 256, 256];
            var csv = new StringBuilder();
            Thread[] threads = new Thread[x];
            List<string[]> file = new List<string[]>();
            int lineCount = 0;
            foreach( var line in File.ReadLines(path))
            {
                file.Add(line.Split(','));
                lineCount = lineCount + 1;
            }
           
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            for(int i=0;i<x;i++){
                int fromLine = 0;
                int linesPerThread = lineCount / x;
                
                if(x == 1) {
                    fromLine = 0;
                    linesPerThread = lineCount;
                }
                else if(i== 0 && x > 1){
                    fromLine = 0;
                    linesPerThread = lineCount / x;
                }
                else if(i == x - 1)
                {
                    fromLine = linesPerThread * i;
                    linesPerThread = lineCount;
                }   
                else{
                    fromLine = linesPerThread * i;
                    linesPerThread = lineCount / x * (i + 1);
                }

                threads[i] = new Thread(()=>CSVFile.ReadFromCsv(1000, fromLine, linesPerThread, file, colorsMatrix));
                threads[i].Name = i + " Thread";
                threads[i].Start();
            }

            for (int i = 0; i < x; i++)
            {
                threads[i].Join();
            }
             stopWatch.Stop();
            //var file = new List<string[]>();
        
           
            //int[,,] colorsMatrix = new int[256, 256, 256];

            //foreach(var line in file)
            //{
              //  var r = Int32.Parse(line[2]);
               // var g = Int32.Parse(line[3]);
                //var b = Int32.Parse(line[4]);
                //colorsMatrix[r,g,b] += 1;
            //}
       
            
        
           
            var counter = 0;
    
            Dictionary<string,int> myDict= new Dictionary<string,int>();

            
            StreamWriter fileTest = new StreamWriter(@"pixogram.csv", false);
            for (int i = 0; i < 256; i++)
            {
                for (int j = 0; j < 256; j++)
                {
                    for (int k = 0; k < 256; k++)
                    {
                        var value = colorsMatrix[i, j, k];
                        if(value != 0)
                        {
                            counter+=1;
                            var newLine = string.Format("{0},{1},{2},{3}", i, j, k, value);
                            myDict.Add(newLine, value);
                        }   

                            
                            //fileTest.WriteLine(newLine);
                    }
                }
            }
            var ordered = myDict.OrderByDescending(key => key.Value);
            var firstRgb = ordered.First().Key;
            var valueSum = myDict.Sum(key => key.Value);
            
            foreach(var line in ordered){
                fileTest.WriteLine(line.Key,line.Value.ToString());
            }
            fileTest.Close();
            CSVFile.generatePixogramRaport(x,stopWatch.Elapsed,counter,valueSum,firstRgb);
   
            //File.WriteAllText(@"sciernisko.csv", csv.ToString());
            
            //foreach (var line in file)
            //{
            //    int red = int.Parse(line[2]) / 16;
            //    int green = int.Parse(line[3]) / 16;
            //    int blue = int.Parse(line[4]) / 16;
            //    if (colors["RED"].ContainsKey(red))
            //    {
            //        colors["RED"][red]++;
            //    }
            //    else
            //    {
            //        colors["RED"][red] = 1;
            //    }
            //    if (colors["GREEN"].ContainsKey(green))
            //    {
            //        colors["GREEN"][green]++;
            //    }
            //    else
            //    {
            //        colors["GREEN"][green] = 1;
            //    }
            //    if (colors["BLUE"].ContainsKey(blue))
            //    {
            //        colors["BLUE"][blue]++;
            //    }
            //    else
            //    {
            //        colors["BLUE"][blue] = 1;
            //    }
            //}
            //var series = new Dictionary<string, System.Windows.Forms.DataVisualization.Charting.Series> {
            //    {
            //        "RED",
            //        new System.Windows.Forms.DataVisualization.Charting.Series {
            //            Name = "R",
            //            Color = Color.Red,
            //            IsVisibleInLegend = true,
            //            ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Column,
            //            MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.None,
            //            BorderWidth = 1,
            //            BorderColor = Color.White
            //        }
            //    },
            //    {
            //        "GREEN",
            //        new System.Windows.Forms.DataVisualization.Charting.Series {
            //            Name = "G",
            //            Color = Color.Green,
            //            IsVisibleInLegend = true,
            //            ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Column,
            //            MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.None,
            //            BorderWidth = 1,
            //            BorderColor = Color.White
            //        }
            //    },
            //    {
            //        "BLUE",
            //        new System.Windows.Forms.DataVisualization.Charting.Series{
            //            Name = "B",
            //            Color = Color.Blue,
            //            IsVisibleInLegend = true,
            //            ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Column,
            //            MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.None,
            //            BorderWidth = 1,
            //            BorderColor = Color.White
            //        }
            //    }
            //};
            //chart.Series.Clear();
            //foreach (var color in colors)
            //{
            //    foreach (var num in color.Value)
            //    {
            //        series[color.Key].Points.AddXY(num.Key, num.Value);
            //    }
            //    chart.Series.Add(series[color.Key]);
            //}
            }

        }
    }
}